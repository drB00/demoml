import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Muser } from '../models/muser.model';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private isAuthenticated = false;
  private _loggedInUser: Muser = null;

  constructor(
    private router: Router
  ) { }

  login(userData) {
    this._loggedInUser = userData.login;
    this.isAuthenticated = true;
  }

  logout() {
    this._loggedInUser = null;
    window.localStorage.removeItem('user');
    this.isAuthenticated = false;
    this.router.navigate(['/login']);
    window.location.reload();
  }

  isLoggedIn(): boolean {
    return this.isAuthenticated;
  }

  get loggedInUser() {
    return this._loggedInUser;
  }

}
