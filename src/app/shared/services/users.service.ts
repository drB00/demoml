import { Injectable } from '@angular/core';
import { BaseApi } from '../core/base-api';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { isNullOrUndefined } from 'util';
import { Muser } from '../models/muser.model';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseApi {

  constructor(
    public http: HttpClient,
    public configService: ConfigService,
  ) {
    super(http, configService);
  }

  /**Функция получает логин пароль или токен из auth */
  async getByLoginPwd(user: Muser, haveToken = false) {
    let login = (haveToken === false) ? await this.login('login', user).toPromise() : user;
    return { login };
  }

}
