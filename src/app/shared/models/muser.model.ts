export class Muser {
  constructor(
    public token?: string,
    public id?: number,
    public login?: string,
    public password?: string,
    public name?: string,
    public surname?: string,
    public usertype?: number,
    public isadmin?: boolean
  ) { }

}

