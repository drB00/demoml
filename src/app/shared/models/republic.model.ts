/**republic – список республик. Он фиксированный и не подлежит изменению.  */
export class Republic {
  constructor(
    public id?: number,
    public name?: string,
  ) { }

}
