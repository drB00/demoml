/**district – список районов */
export class District {
  constructor(
    public id?: number,
    public region_id?: number,
    public name?: string,
  ) { }

}
