/**region – список регионов республики */
export class Region {
  constructor(
    public id?: number,
    public republic_id?: number,
    public name?: string,
  ) { }

}
