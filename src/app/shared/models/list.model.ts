/**list – список списков */
export class List {
  constructor(
    public id?: number,
    public archive?: string,
    public fond?: string,
    public opis?: string,
    public delo?: string,
    // public URL?: string,
    // public url?: string,
    public sheets?: string,
    public preparation_date?: Date,
    public creation_datetime?: Date,
    public rtype?: number
  ) {}
}
