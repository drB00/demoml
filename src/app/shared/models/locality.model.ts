/**locality – список населенных пунктов */
export class Locality {
  constructor(
    public id?: number,
    public district_id?: number,
    public name?: string,
    public district_name?: string,
    public region_name?: string,
    public republic_name?: string,
    public district_center?: boolean, // Районный центр
    public longitude?: number,
    public latitude?: number,
    public checkToDel = false,

  ) { }

}
