import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  MatInputModule,
  MatSlideToggleModule,
  MatButtonModule,
  MatCheckboxModule,
  MatSelectModule,
  MatDatepickerModule,
  MatTooltipModule,
  MatTabsModule,
  MatCardModule,
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatSnackBarModule,
  MatExpansionModule,
  MatSortModule,
  MatDialogModule,
  MatPaginatorModule,
  MAT_DATE_LOCALE,
  DateAdapter,
  MatMenuModule,
  MAT_DATE_FORMATS,
  MatProgressBarModule,
  MatRadioModule
} from '@angular/material';
import { TextMaskModule } from 'angular2-text-mask';

import { HttpClientModule } from '@angular/common/http';

import {
  MomentDateAdapter,
  MatMomentDateModule,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS
} from '@angular/material-moment-adapter';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularEditorModule } from '@kolkov/angular-editor';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY'
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MM YYYY',
    fullPickerInput: 'DD.MM.YYYY',
    datePickerInput: 'DD.MM.YYYY'
  }
};

@NgModule({
  declarations: [],
  imports: [
    AngularEditorModule,
    NgbModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatSortModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatPaginatorModule,
    TextMaskModule,
    MatProgressBarModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatRadioModule
  ],
  exports: [
    AngularEditorModule,
    NgbModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatSortModule,
    MatAutocompleteModule,
    TextMaskModule,
    MatDialogModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatRadioModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class SharedModule {}
