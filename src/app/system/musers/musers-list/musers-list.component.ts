import { Component, OnInit, ViewChild } from '@angular/core';
import { Muser } from 'src/app/shared/models/muser.model';
import { PageEvent, MatPaginator } from '@angular/material';
import { GlobalService } from '../../shared/services/global.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-musers-list',
  templateUrl: './musers-list.component.html',
  styleUrls: ['./musers-list.component.scss']
})
export class MusersListComponent implements OnInit {

  postfixId = '';
  postfixLogin = '';
  searchOrder = 'byid';

  searchStr = '';
  earchOrder = 'byid';
  musers: Muser[];

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent;
  start = 0;
  end = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private globalService: GlobalService,
    private authService: AuthService,
    private muserService: MuserService,
    private router: Router
  ) { }

  async ngOnInit() {
    try {
      let musers = this.muserService.getAll();
      this.musers = (isNullOrUndefined(await musers)) ? [] : await musers;
      this.musers.sort((a, b) => (a.id > b.id) ? 1 : -1);
      this.length = this.musers.length;
      if (this.length > 100) {
        this.pageSizeOptions.push(this.length);
      }
      this.end = this.pageSize;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.searchOrder = isNullOrUndefined(this.globalService.muserFilter.searchOrder) ? 'byid' :
        this.globalService.muserFilter.searchOrder;
      this.setOrgerStr();
    }
  }

  updateData() {
    if (this.searchStr.length > 0) {
      this.start = 0;
      this.end = this.paginator.pageSize;
      this.paginator.firstPage();
    } else {
      this.start = this.paginator.pageSize * this.paginator.pageIndex;
      this.end = this.start + this.paginator.pageSize;
    }
  }

  setPageSizeOptions(event: PageEvent) {
    this.start = event.pageSize * event.pageIndex;
    this.end = this.start + event.pageSize;
    // this.airusersView = this.musers.slice(this.start, this.end);
    return event;
  }

  onAddUser() {
    this.router.navigate(['/sys/users/edit/newprofile']);
  }

  onEditUser(id) {
    this.router.navigate([`/sys/users/edit/${id}`]);
  }

  trim(content) {
    let end: number;
    let str: string;
    if (document.documentElement.offsetWidth > 650) {
      end = Math.floor((document.documentElement.offsetWidth - 300) / 60);
    } else {
      end = Math.floor((document.documentElement.offsetWidth) / 60);
    }
    if (content.length > end + 2) {
      str = content.substring(0, end);
    } else {
      str = content;
    }
    return { str, end };
  }

  selectOrder(type: string) {
    switch (type) {
      case 'bylogin':
        this.searchOrder = (this.searchOrder === 'bylogin') ? 'bylogindesc' : 'bylogin';
        break;
      case 'byid':
        this.searchOrder = (this.searchOrder === 'byid') ? 'byiddesc' : 'byid';
        break;
    }
    this.setOrgerStr();
    this.selectFilter();
  }

  setOrgerStr() {
    switch (this.searchOrder) {
      case 'byid':
        this.postfixId = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixLogin = '';
        this.musers.sort((a, b) => (a.id > b.id) ? 1 : -1);
        break;
      case 'byiddesc':
        this.postfixId = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixLogin = '';
        this.musers.sort((a, b) => (a.id < b.id) ? 1 : -1);
        break;
      case 'bylogin':
        this.postfixId = '';
        this.postfixLogin = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.musers.sort((a, b) => (a.login > b.login) ? 1 : -1);
        break;
      case 'bylogindesc':
        this.postfixId = '';
        this.postfixLogin = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.musers.sort((a, b) => (a.login < b.login) ? 1 : -1);
        break;
    }
  }

  selectFilter() {
    this.globalService.muserFilter = {
      searchStr: this.searchStr, searchOrder: this.searchOrder
    };
    // if (sort === true) {
    //   this.musers.sort((a, b) => (a.login > b.login) ? 1 : -1);
    // }
    // this.getSearchOpisId();
    // if ((this.searchOpis.length > 0 || this.searchFond.length > 0) && this.searchOpisId.length === 0) {
    //   this.archivefolders.length = 0;
    //   this.length = 0;
    // } else {
    //   if (event !== false) {
    //     this.getArchivefolders();
    //   }
    // }
    // this.setOrgerStr();
  }


}
