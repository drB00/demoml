import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-musers',
  templateUrl: './musers.component.html',
  styleUrls: ['./musers.component.scss']
})
export class MusersComponent implements OnInit {

  constructor(
    private router: Router,
  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url === '/sys/users') {
          this.router.navigate(['/sys/users/list']);
        }
      }
    });
  }

  ngOnInit() {
  }

}
