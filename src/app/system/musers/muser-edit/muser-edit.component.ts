import { Component, OnInit } from '@angular/core';
import { Muser } from 'src/app/shared/models/muser.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { GlobalService } from '../../shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { isNullOrUndefined, isUndefined } from 'util';
import { DialogYesNoComponent } from '../../shared/components/dialogs/dialog-yes-no/dialog-yes-no.component';

@Component({
  selector: 'app-muser-edit',
  templateUrl: './muser-edit.component.html',
  styleUrls: ['./muser-edit.component.scss']
})
export class MuserEditComponent implements OnInit {

  id: number;
  intType = ''; // тип интерфейса
  loggedInUser: Muser;
  selectedUser: Muser;
  musers: Muser[];
  muserForm: FormGroup;
  accessLogin = true;

  readonly = true;

  constructor(
    private authService: AuthService,
    private muserService: MuserService,
    private globalService: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activatedRoute.params.subscribe(param => {
      this.id = param.id;
      if (param.id === 'myprofile') {
        this.intType = 'myprofile';
        this.ngOnInit();
      } else if (param.id === 'newprofile') {
        this.intType = 'newprofile';
      } else {
        this.id = +param.id;
        this.intType = 'editprofile';
      }
    });
  }

  async ngOnInit() {
    this.muserForm = new FormGroup({
      'isadmin': new FormControl(false),
      'login': new FormControl(null, [Validators.required]),
      'name': new FormControl(null, [Validators.required]),
      'surname': new FormControl(null, [Validators.required]),
      'password': (this.intType === 'newprofile') ? new FormControl(null, [Validators.required]) : new FormControl(null)
    });
    try {
      let loggedInUser = this.muserService.getOneById(this.authService.loggedInUser.id);
      let musers = this.muserService.getAll();
      this.loggedInUser = await loggedInUser;
      this.musers = (isNullOrUndefined(await musers)) ? [] : await musers;
      this.getData();
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
  }

  async getData() {
    switch (this.intType) {
      case 'myprofile':
        this.muserForm.patchValue({
          'name': this.loggedInUser.name,
          'surname': this.loggedInUser.surname,
          'login': this.loggedInUser.login,
          'isadmin': this.loggedInUser.isadmin
        });
        break;
      case 'newprofile':
        this.muserForm.patchValue({
          'name': '',
          'surname': '',
          'login': '',
          'password': null,
          'isadmin': false
        });
        break;
      case 'editprofile':
        this.selectedUser = this.musers.find(muser => muser.id === this.id);
        this.muserForm.patchValue({
          'name': this.selectedUser.name,
          'surname': this.selectedUser.surname,
          'login': this.selectedUser.login,
          'isadmin': this.selectedUser.isadmin,
          'password': null
        });
        break;
      default:
    }
  }

  async onSaveMuserForm() {
    if (this.musers.filter(muser => muser.login === this.muserForm.value.login).length > 0 &&
      (this.muserForm.value['login'] !== this.selectedUser.login)) {
      this.openSnackBar(`Этот логин уже занят.`, 'Ok');
      return;
    }
    switch (this.intType) {
      case 'newprofile':
        try {
          let resPostMuser = await this.muserService.postOne(this.muserForm.value);
          this.musers.push(resPostMuser);
          this.getData();
          this.readonly = true;
          this.openSnackBar(`Пользователь добавлен.`, 'Ok');
          this.router.navigate(['/sys/users/list']);
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
        break;
      case 'editprofile':
        if (this.muserForm.value['password'] == null || this.muserForm.value['password'] === '') {
          this.muserForm.removeControl('password');
        }
        try {
          let resPutMuser = await this.muserService.putOneById(this.id, this.muserForm.value);
          this.musers.splice(this.musers.findIndex(muser => muser.id === resPutMuser.id), 1);
          this.musers.push(resPutMuser);
          this.getData();
          this.openSnackBar(`Сохранено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          if (isUndefined(this.muserForm.value['password'])) {
            this.muserForm.addControl('password', new FormControl(null));
          }
        }
        break;
      default:
        break;
    }
  }

  async onDelMuser() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить пользователя',
        question: `${this.selectedUser.name} ${this.selectedUser.surname}?`
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === true) {
        try {
          let resDelMuser = await this.muserService.deleteOneById(this.id);
          this.router.navigate(['/sys/users/list']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении пользователя ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }

}
