import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { PagecontentService } from '../shared/services/pagecontent.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
  htmlContent = '';
  // editorConfig: AngularEditorConfig = {
  //   editable: true,
  //   spellcheck: true,
  //   translate: 'no',
  //   uploadUrl: 'v2/images',
  //   fonts: [{ name: 'Roboto', class: 'roboto' }]
  // };
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: '',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [{ name: 'Roboto', class: 'roboto' }],
    uploadUrl: 'v2/image',
    sanitize: true,
    toolbarPosition: 'top'
  };

  constructor(private pagecontentService: PagecontentService) {}

  async ngOnInit() {
    let _contents = this.pagecontentService.getAll();
    let contents = isNullOrUndefined(await _contents) ? [] : await _contents;

    this.htmlContent = contents[0].content;
  }
  async onSave() {
    let res = await this.pagecontentService.putOneById(1, {
      content: this.htmlContent
    });
  }
}
