import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemComponent } from './system.component';
import { MusersListComponent } from './musers/musers-list/musers-list.component';
import { MuserEditComponent } from './musers/muser-edit/muser-edit.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { AuthGuard } from '../shared/services/auth.guard';
import { MusersComponent } from './musers/musers.component';
import { ListofsComponent } from './listofs/listofs.component';
import { ListofsListComponent } from './listofs/listofs-list/listofs-list.component';
import { ListofsEditComponent } from './listofs/listofs-edit/listofs-edit.component';
import { GeoobjectsComponent } from './geoobjects/geoobjects.component';
import { GeoobjectsListComponent } from './geoobjects/geoobjects-list/geoobjects-list.component';
import { FilesComponent } from './files/files.component';
import { FilesListComponent } from './files/files-list/files-list.component';
import { FilesEditComponent } from './files/files-edit/files-edit.component';
import { EditorComponent } from './editor/editor.component';

const routes: Routes = [
  {
    path: 'sys',
    data: { breadcrumb: '' },
    component: SystemComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'users',
        data: { breadcrumb: 'Пользователи' },
        component: MusersComponent,
        children: [
          {
            path: 'list',
            data: { breadcrumb: '' },
            component: MusersListComponent
          },
          {
            path: 'edit/:id',
            data: { breadcrumb: 'Карточка пользователя' },
            component: MuserEditComponent
          }
        ]
      },
      {
        path: 'listof',
        data: { breadcrumb: 'Списки' },
        component: ListofsComponent,
        children: [
          {
            path: 'list',
            data: { breadcrumb: '' },
            component: ListofsListComponent
          },
          {
            path: 'edit/:id',
            data: { breadcrumb: 'Редактор списка' },
            component: ListofsEditComponent
          }
        ]
      },
      {
        path: 'geo',
        data: { breadcrumb: 'Географические объекты' },
        component: GeoobjectsComponent,
        children: [
          {
            path: 'list',
            data: { breadcrumb: '' },
            component: GeoobjectsListComponent
          }
        ]
      },
      {
        path: 'files',
        data: { breadcrumb: 'Файлы' },
        component: FilesComponent,
        children: [
          {
            path: 'list',
            data: { breadcrumb: '' },
            component: FilesListComponent
          },
          {
            path: 'edit/:id',
            data: { breadcrumb: 'Редактор изображения' },
            component: FilesEditComponent
          }
        ]
      },
      {
        path: 'editor',
        data: { breadcrumb: 'Редактор' },
        component: EditorComponent
      }
    ]
  },
  {
    path: '**',
    data: { breadcrumb: '404' },
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule {}
