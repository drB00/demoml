import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatPaginator, PageEvent } from '@angular/material';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import { ListService } from '../../shared/services/list.service';
import { List } from 'src/app/shared/models/list.model';

@Component({
  selector: 'app-dialog-locality',
  templateUrl: './dialog-locality.component.html',
  styleUrls: ['./dialog-locality.component.scss']
})
export class DialogLocalityComponent implements OnInit {

  map: any;
  marker;

  lists: List[];
  length = 0;
  pageSize = 7;
  pageSizeOptions: number[] = [7];
  pageEvent: PageEvent;
  start = 0;
  end = 7;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private listService: ListService,
    private router: Router,
    public dialogRef: MatDialogRef<DialogLocalityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  async ngOnInit() {
    this.map = L.map('mapid').setView([
      (this.data.latitude === '') ? 55.75455911564169 : this.data.latitude,
      (this.data.longitude === '') ? 37.61931572776302 : this.data.longitude
    ], 9);
    this.map.on('click', (event) => {
      if (this.data.viewOnly !== true) {
        this.data.latitude = event.latlng.lat.toFixed(7);
        this.data.longitude = event.latlng.lng.toFixed(7);
        if (!isNullOrUndefined(this.marker)) {
          this.map.removeLayer(this.marker);
        }
        this.marker = L.marker([this.data.latitude, this.data.longitude]).addTo(this.map);
      }
    });

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    if (this.data.edit === true) {
      this.marker = L.marker([this.data.latitude, this.data.longitude]).addTo(this.map);
      try {
        let lists = this.listService.getBylocality(this.data.id);
        let length = this.listService.getBylocality_count(this.data.id);
        this.lists = (isNullOrUndefined(await lists)) ? [] : await lists;
        this.length = (await length).rowcount;
      } catch (err) {
        console.error(err);
      }
    }
  }

  gotoList(id) {
    this.router.navigate([`/sys/listof/edit/${id}`]);
    this.dialogRef.close('location');
  }

  setFlag(event) {
    this.data.myFlagForButtonToggle = event.value;
  }

  setPageSizeOptions(event: PageEvent) {
    this.start = event.pageSize * event.pageIndex;
    this.end = this.start + event.pageSize;
    return event;
  }

}

declare let L;
