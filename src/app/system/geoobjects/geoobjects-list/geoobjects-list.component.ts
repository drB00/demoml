import { Component, OnInit, ViewChild } from '@angular/core';
import { Region } from 'src/app/shared/models/region.model';
import { Republic } from 'src/app/shared/models/republic.model';
import { District } from 'src/app/shared/models/district.model';
import { Locality } from 'src/app/shared/models/locality.model';
import {
  MatSnackBar,
  MatDialog,
  PageEvent,
  MatPaginator
} from '@angular/material';
import { AuthService } from 'src/app/shared/services/auth.service';
import { LocalityService } from '../../shared/services/locality.service';
import { RepublicService } from '../../shared/services/republic.service';
import { RegionService } from '../../shared/services/region.service';
import { DistrictService } from '../../shared/services/district.service';
import { isNullOrUndefined } from 'util';
import { DialogSetstringComponent } from '../../shared/components/dialogs/dialog-setstring/dialog-setstring.component';
import { DialogLocalityComponent } from '../dialog-locality/dialog-locality.component';
import { GlobalService } from '../../shared/services/global.service';

@Component({
  selector: 'app-geoobjects-list',
  templateUrl: './geoobjects-list.component.html',
  styleUrls: ['./geoobjects-list.component.scss']
})
export class GeoobjectsListComponent implements OnInit {
  republics: Republic[];
  selectedRepublicId = -1;
  republicsView: Republic[] = [];
  regions: Region[];
  selectedRegionId = -1;
  regionsView: Region[] = [];
  districts: District[];
  selectedDistrictId = -1;
  districtsView: District[] = [];
  localitys: Locality[];
  localitysView: Locality[];

  searchRepublic = '';
  postfixRepublic = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
  orderRepublic = 'byname';

  searchRegion = '';
  postfixRegion = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
  orderRegion = 'byname';
  searchDistrict = '';
  postfixDistrict = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
  orderDistrict = 'byname';
  searchLocality = '';
  postfixLocality = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
  orderLocality = 'byname';

  // MatPaginator republic
  republicsLength = 0;
  republic_pageSize = 5;
  republic_pageSizeOptions: number[] = [5, 10, 25, 100];
  republic_pageEvent: PageEvent;
  republic_start = 0;
  republic_end = 5;
  @ViewChild('republic_MatPaginator') republic_paginator: MatPaginator;
  // MatPaginator region
  regionsLength = 0;
  region_pageSize = 5;
  region_pageSizeOptions: number[] = [5, 10, 25, 100];
  region_pageEvent: PageEvent;
  region_start = 0;
  region_end = 5;
  @ViewChild('region_MatPaginator') region_paginator: MatPaginator;
  // MatPaginator district
  districtsLength = 0;
  district_pageSize = 5;
  district_pageSizeOptions: number[] = [5, 10, 25, 100];
  district_pageEvent: PageEvent;
  district_start = 0;
  district_end = 5;
  @ViewChild('district_MatPaginator') district_paginator: MatPaginator;
  // MatPaginator locality
  localitysLength = 0;
  locality_pageSize = 5;
  locality_pageSizeOptions: number[] = [5, 10, 25, 100];
  locality_pageEvent: PageEvent;
  locality_start = 0;
  locality_end = 5;
  @ViewChild('locality_MatPaginator') locality_paginator: MatPaginator;

  constructor(
    private authService: AuthService,
    private globalService: GlobalService,
    private republicService: RepublicService,
    private regionService: RegionService,
    private districtService: DistrictService,
    private localityService: LocalityService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.getData();
  }

  async getData() {
    this.selectedRepublicId = -1;
    this.selectedRegionId = -1;
    this.selectedDistrictId = -1;
    try {
      let republics = this.republicService.getWithOrder();
      let regions = this.regionService.getWithOrder();
      let districts = this.districtService.getWithOrder();
      this.republics = isNullOrUndefined(await republics)
        ? []
        : await republics;
      this.regions = isNullOrUndefined(await regions) ? [] : await regions;
      this.districts = isNullOrUndefined(await districts)
        ? []
        : await districts;
      this.republicsView = this.republics;
      // this.regionsView = this.regions;
      // this.districtsView = this.districts;
      this.republicsLength = this.republics.length;
      // this.regionsLength = this.regions.length;
      // this.districtsLength = this.districts.length;
      // this.selectedRepublicId = this.globalService.geoobjectFilter.selectedRepublicId;
      // this.selectedRegionId = this.globalService.geoobjectFilter.selectedRegionId;
      // this.selectedDistrictId = this.globalService.geoobjectFilter.selectedDistrictId;
      if (this.globalService.geoobjectFilter.selectedLocalityId > -1) {
        this.onSelectRepublic(
          this.globalService.geoobjectFilter.selectedRepublicId
        );
        this.onSelectRegion(
          this.globalService.geoobjectFilter.selectedRegionId
        );
        await this.onSelectDistrict(
          this.globalService.geoobjectFilter.selectedDistrictId
        );
        this.onEditLocality(
          this.globalService.geoobjectFilter.selectedLocalityId,
          'list'
        );
      }
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
    }
  }

  // resetSavefilter() {
  //   let obj = {
  //     selectedRepublicId: -1,
  //     selectedRegionId: -1,
  //     selectedDistrictId: -1,
  //     selectedLocalityId: -1
  //   };
  //   this.globalService.geoobjectFilter = obj;
  // }

  onSelectRepublic(id) {
    this.selectedRepublicId = id;
    this.onSelectRegion(-1);
    if (id === -1) {
      this.regionsView = [];
      this.regionsLength = 0;
    } else {
      this.regionsView = this.regions.filter(item => item.republic_id === id);
      if (this.orderRegion === 'byname') {
        this.regionsView.sort((a, b) => (a.name > b.name ? 1 : -1));
      } else {
        this.regionsView.sort((a, b) => (a.name < b.name ? 1 : -1));
      }
      this.regionsLength = this.regionsView.length;
    }
  }
  onSelectRegion(id) {
    this.selectedRegionId = id;
    this.onSelectDistrict(-1);
    if (id === -1) {
      this.districtsView = [];
      this.districtsLength = 0;
    } else {
      this.districtsView = this.districts.filter(item => item.region_id === id);
      if (this.orderDistrict === 'byname') {
        this.districtsView.sort((a, b) => (a.name > b.name ? 1 : -1));
      } else {
        this.districtsView.sort((a, b) => (a.name < b.name ? 1 : -1));
      }
      this.districtsLength = this.districtsView.length;
    }
  }
  async onSelectDistrict(id) {
    this.selectedDistrictId = id;
    if (id === -1) {
      this.selectedDistrictId = -1;
      this.localitysView = [];
      this.localitysLength = 0;
    } else {
      let localitysView = this.localityService.getByDistrict(id);
      this.localitysView = isNullOrUndefined(await localitysView)
        ? []
        : await localitysView;
      if (this.orderLocality === 'byname') {
        this.localitysView.sort((a, b) => (a.name > b.name ? 1 : -1));
      } else {
        this.localitysView.sort((a, b) => (a.name < b.name ? 1 : -1));
      }
      this.localitysLength = this.localitysView.length;
    }
  }

  onAddRepublic() {
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: 'Добавить республику'
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== false) {
        try {
          await this.republicService.postOne({ name: result });
          this.getData();
        } catch (err) {
          console.error(`Ошибка при добавлении республики ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      }
    });
  }
  onEditRepublic(id, event: Event) {
    event.stopPropagation();
    let republicidx = this.republicsView.findIndex(item => item.id === id);
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: 'Переименовать республику',
        str: this.republicsView[republicidx].name
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== false) {
        try {
          await this.republicService.putOneById(id, { name: result });
          this.republicsView[republicidx].name = result;
        } catch (err) {
          console.error(`Ошибка при переименовании республики ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      }
    });
  }
  onAddRegion() {
    let republicName = this.republics.find(
      item => item.id === this.selectedRepublicId
    ).name;
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Добавить регион в республику ${republicName}`
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== false) {
        try {
          let postregion = await this.regionService.postOne({
            name: result,
            republic_id: this.selectedRepublicId
          });
          let regions = this.regionService.getAll();
          this.regions = isNullOrUndefined(await regions) ? [] : await regions;
          this.regionsView.push(postregion);
          this.regionsView.sort((a, b) => a.id - b.id);
          this.regionsLength = this.regionsView.length;
          // this.getData();
        } catch (err) {
          console.error(`Ошибка при добавлении региона ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      }
    });
  }
  onEditRegion(id) {
    event.stopPropagation();
    let regionidx = this.regionsView.findIndex(item => item.id === id);
    let republicName = this.republics.find(
      item => item.id === this.regionsView[regionidx].republic_id
    ).name;
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Переименовать регион в республике ${republicName}`,
        str: this.regionsView[regionidx].name,
        edit: true
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (
        !isNullOrUndefined(result) &&
        result !== false &&
        result !== 'delete'
      ) {
        try {
          await this.regionService.putOneById(id, { name: result });
          let regions = this.regionService.getAll();
          this.regions = isNullOrUndefined(await regions) ? [] : await regions;
          this.regionsView[regionidx].name = result;
        } catch (err) {
          console.error(`Ошибка при переименовании региона ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      } else if (result === 'delete') {
        try {
          await this.regionService.deleteOneById(id);
          let regions = this.regionService.getAll();
          this.regions = isNullOrUndefined(await regions) ? [] : await regions;
          this.regionsView.splice(regionidx, 1);
          // this.regions.splice(regionidx, 1);
          this.regionsLength = this.regionsView.length;
        } catch (err) {
          if (err.status === 409) {
            this.openSnackBar(`${err.error['error']}`, 'Ok');
          } else {
            console.error(`Ошибка при удалении региона ${err}`);
            this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
          }
        }
      }
    });
  }

  onAddDistrict() {
    let regionName = this.regions.find(
      item => item.id === this.selectedRegionId
    ).name;
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Добавить район в регионе ${regionName}`
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== false) {
        try {
          let postdistrict = await this.districtService.postOne({
            name: result,
            region_id: this.selectedRegionId
          });
          let districts = this.districtService.getAll();
          this.districts = isNullOrUndefined(await districts)
            ? []
            : await districts;
          this.districtsView.push(postdistrict);
          this.districtsView.sort((a, b) => a.id - b.id);
          this.districtsLength = this.districtsView.length;
          // this.getData();
        } catch (err) {
          console.error(`Ошибка при добавлении района ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      }
    });
  }
  onEditDistrict(id) {
    event.stopPropagation();
    let districtidx = this.districtsView.findIndex(item => item.id === id);
    let regionName = this.regions.find(
      item => item.id === this.districtsView[districtidx].region_id
    ).name;
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Переименовать район в регионе ${regionName}`,
        str: this.districtsView[districtidx].name,
        edit: true
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (
        !isNullOrUndefined(result) &&
        result !== false &&
        result !== 'delete'
      ) {
        try {
          await this.districtService.putOneById(id, { name: result });
          let districts = this.districtService.getAll();
          this.districts = isNullOrUndefined(await districts)
            ? []
            : await districts;
          this.districtsView[districtidx].name = result;
        } catch (err) {
          console.error(`Ошибка при переименовании района ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      } else if (result === 'delete') {
        try {
          await this.districtService.deleteOneById(id);
          let districts = this.districtService.getAll();
          this.districts = isNullOrUndefined(await districts)
            ? []
            : await districts;
          this.districtsView.splice(districtidx, 1);
          // this.regions.splice(regionidx, 1);
          this.districtsLength = this.districtsView.length;
        } catch (err) {
          if (err.status === 409) {
            this.openSnackBar(`${err.error['error']}`, 'Ok');
          } else {
            console.error(`Ошибка при удалении района ${err}`);
            this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
          }
        }
      }
    });
  }

  onAddLocality() {
    let districtName = this.districts.find(
      item => item.id === this.selectedDistrictId
    ).name;
    let dialogRef = this.dialog.open(DialogLocalityComponent, {
      panelClass: 'app-dialog-full',
      data: {
        title: `Добавить населенный пункт в район ${districtName}`,
        myFlagForButtonToggle: 'map',
        district_center: false,
        longitude: '',
        latitude: ''
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== false) {
        try {
          let postlocality = await this.localityService.postOne({
            name: result.name,
            district_id: this.selectedDistrictId,
            district_center: result.district_center,
            longitude: result.longitude,
            latitude: result.latitude
          });
          let localitysView = this.localityService.getByDistrict(
            this.selectedDistrictId
          );
          this.localitysView = isNullOrUndefined(await localitysView)
            ? []
            : await localitysView;
          this.localitysLength = this.localitysView.length;
        } catch (err) {
          console.error(`Ошибка при добавлении населенного пункта ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      }
    });
  }

  onEditLocality(id, myFlagForButtonToggle = 'map') {
    event.stopPropagation();
    let localityidx = this.localitysView.findIndex(item => item.id === id);
    let districtName = this.districts.find(
      item => item.id === this.selectedDistrictId
    ).name;
    let dialogRef = this.dialog.open(DialogLocalityComponent, {
      panelClass: 'app-dialog-full',
      data: {
        myFlagForButtonToggle,
        id,
        title: `Редактировать населенный пункт в районе ${districtName}`,
        edit: true,
        name: this.localitysView[localityidx].name,
        district_center: this.localitysView[localityidx].district_center,
        longitude: this.localitysView[localityidx].longitude,
        latitude: this.localitysView[localityidx].latitude
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      this.globalService.resetgeoobjectFilter();
      if (
        !isNullOrUndefined(result) &&
        result !== false &&
        result !== 'delete' &&
        result !== 'location'
      ) {
        try {
          let putlocality = await this.localityService.putOneById(id, {
            name: result.name,
            district_id: this.selectedDistrictId,
            district_center: result.district_center,
            longitude: result.longitude,
            latitude: result.latitude
          });
          let localitysView = this.localityService.getByDistrict(
            this.selectedDistrictId
          );
          this.localitysView = isNullOrUndefined(await localitysView)
            ? []
            : await localitysView;
          this.localitysLength = this.localitysView.length;
        } catch (err) {
          console.error(`Ошибка при добавлении населенного пункта ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      } else if (result === 'delete') {
        try {
          await this.localityService.deleteOneById(id);
          let localitysView = this.localityService.getByDistrict(
            this.selectedDistrictId
          );
          this.localitysView = isNullOrUndefined(await localitysView)
            ? []
            : await localitysView;
          this.localitysLength = this.localitysView.length;
        } catch (err) {
          if (err.status === 409) {
            this.openSnackBar(`${err.error['error']}`, 'Ok');
          } else {
            console.error(`Ошибка при удалении населенного пункта ${err}`);
            this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
          }
        }
      } else if (result === 'location') {
        let obj = {
          selectedRepublicId: this.selectedRepublicId,
          selectedRegionId: this.selectedRegionId,
          selectedDistrictId: this.selectedDistrictId,
          selectedLocalityId: id
        };
        this.globalService.geoobjectFilter = obj;
      }
    });
  }

  republic_setPageSizeOptions(event) {
    this.republic_start = event.pageSize * event.pageIndex;
    this.republic_end = this.republic_start + event.pageSize;
    return event;
  }
  region_setPageSizeOptions(event) {
    this.region_start = event.pageSize * event.pageIndex;
    this.region_end = this.region_start + event.pageSize;
    return event;
  }
  district_setPageSizeOptions(event) {
    this.district_start = event.pageSize * event.pageIndex;
    this.district_end = this.district_start + event.pageSize;
    return event;
  }
  locality_setPageSizeOptions(event) {
    this.locality_start = event.pageSize * event.pageIndex;
    this.locality_end = this.locality_start + event.pageSize;
    return event;
  }

  selectOrderRepublic() {
    if (this.orderRepublic === 'byname') {
      this.orderRepublic = 'bynamedesc';
      this.postfixRepublic =
        '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
      this.republicsView.sort((a, b) => (a.name < b.name ? 1 : -1));
    } else {
      this.orderRepublic = 'byname';
      this.postfixRepublic =
        '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
      this.republicsView.sort((a, b) => (a.name > b.name ? 1 : -1));
    }
  }
  selectOrderRegion() {
    if (this.orderRegion === 'byname') {
      this.orderRegion = 'bynamedesc';
      this.postfixRegion =
        '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
      this.regionsView.sort((a, b) => (a.name < b.name ? 1 : -1));
    } else {
      this.orderRegion = 'byname';
      this.postfixRegion =
        '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
      this.regionsView.sort((a, b) => (a.name > b.name ? 1 : -1));
    }
  }
  selectOrderDistrict() {
    if (this.orderDistrict === 'byname') {
      this.orderDistrict = 'bynamedesc';
      this.postfixDistrict =
        '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
      this.districtsView.sort((a, b) => (a.name < b.name ? 1 : -1));
    } else {
      this.orderDistrict = 'byname';
      this.postfixDistrict =
        '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
      this.districtsView.sort((a, b) => (a.name > b.name ? 1 : -1));
    }
  }
  selectOrderLocality() {
    if (this.orderLocality === 'byname') {
      this.orderLocality = 'bynamedesc';
      this.postfixLocality =
        '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
      this.localitysView.sort((a, b) => (a.name < b.name ? 1 : -1));
    } else {
      this.orderLocality = 'byname';
      this.postfixLocality =
        '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
      this.localitysView.sort((a, b) => (a.name > b.name ? 1 : -1));
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000
    });
  }
}
