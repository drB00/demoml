import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { GlobalService } from '../shared/services/global.service';

@Component({
  selector: 'app-geoobjects',
  templateUrl: './geoobjects.component.html',
  styleUrls: ['./geoobjects.component.scss']
})
export class GeoobjectsComponent implements OnInit {

  constructor(
    private globalService: GlobalService,
    private router: Router,
  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url === '/sys/geo') {
          this.globalService.resetgeoobjectFilter();
          this.router.navigate(['/sys/geo/list']);
        }
      }
    });
  }

  ngOnInit() {
  }

}
