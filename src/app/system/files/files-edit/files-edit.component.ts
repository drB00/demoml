import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageCropperComponent, ImageCroppedEvent } from 'ngx-image-cropper';
import { ListfileService } from '../../shared/services/listfile.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-files-edit',
  templateUrl: './files-edit.component.html',
  styleUrls: ['./files-edit.component.scss']
})
export class FilesEditComponent implements OnInit {

  id: number;
  filename = '';

  show = false;
  croppedImage: any;
  imgfile: any;
  maxHeight = 500;

  @ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent;

  constructor(
    private listfileService: ListfileService,
    private location: Location,
    private spinnerService: Ng4LoadingSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(param => {
      this.id = param.id;
    });
  }

  async ngOnInit() {
    try {
      this.spinnerService.show();
      this.maxHeight = window.innerHeight - 320;
      let listfile = this.listfileService.getOneById(this.id);
      let file = await this.listfileService.getFileById(this.id);
      let resfile = new Blob([file], { type: 'image/jpg' });
      let reader = new FileReader();
      reader.readAsDataURL(resfile);
      reader.onloadend = () => {
        let base64data = String(reader.result);
        this.imgfile = base64data;
        this.show = true;
      };
      this.filename = (await listfile).listfile.filename;
    } catch (err) {
      console.error(err);
    } finally {
      this.spinnerService.hide();
    }

  }

  fileChangeEvent(event: any): void {
    // this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.file;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  rotateLeft() {
    this.imageCropper.rotateLeft();
  }
  rotateRight() {
    this.imageCropper.rotateRight();
  }
  flipHorizontal() {
    this.imageCropper.flipHorizontal();
  }
  flipVertical() {
    this.imageCropper.flipVertical();
  }

  async onSave() {
    this.spinnerService.show();
    try {
      this.croppedImage['lastModifiedDate'] = new Date();
      let formData: FormData = new FormData();
      formData.append('listfile', this.croppedImage, this.filename);
      await this.listfileService.putOneById(this.id, formData);
      this.location.back();
    } catch (err) {
      console.error(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  onGoback() {
    this.location.back();
  }

}
