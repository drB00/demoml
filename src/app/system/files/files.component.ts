import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { GlobalService } from '../shared/services/global.service';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {

  constructor(
    private globalService: GlobalService,
    private router: Router
  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url === '/sys/files') {
          this.globalService.fileslistFilter.fileslistsetImgIdx = -1;
          this.globalService.fileslistFilter.fileslistsetImgIdxCount = 0;
          this.router.navigate(['/sys/files/list']);
        }
      }
    });
  }

  ngOnInit() {
  }

}
