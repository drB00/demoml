import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAction, NgxGalleryComponent, NgxGalleryAnimation } from 'ngx-gallery';
import { ListfileService } from '../../shared/services/listfile.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GlobalService } from '../../shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { saveAs as importedSaveAs } from 'file-saver';
import { HttpEventType, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.scss']
})
export class FilesListComponent implements OnInit {

  listfiles: any[];
  _listfiles: any[];

  searchOrder = 'byid';
  postfixId = '';
  postfixCrtime = '';
  postfixShifr = '';
  postfixFilename = '';

  filter_fond: string;
  filter_opis_nam: string;
  filter_delo: string;
  filter_filename: string;

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent;
  start = 0;
  end = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  inProgress = false;
  inProgressVal = 0;

  /**Галерея */
  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];
  galleryAction: NgxGalleryAction[] = [];
  @ViewChild('gallary') gallary: NgxGalleryComponent;
  showGallary = true;
  setImgIdx: number; // индекс картинки,открытой в галерее
  setImgId: number; // id открытая в галерее
  setListId: number; // id дела в галерее
  openImgIdx = -1; // idx изображения с которого галерея открыта
  openImgIdxCount = 0; // инкримент к idx изображения с которого галерея открыта

  imgBufsize = 10; // /2 буфер изображений в галерее


  constructor(
    private authService: AuthService,
    private listfileService: ListfileService,
    private globalService: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  async ngOnInit() {
    this.searchOrder = this.globalService.fileslistFilter.searchOrder;
    this.filter_fond = this.globalService.fileslistFilter.filter_fond;
    this.filter_opis_nam = this.globalService.fileslistFilter.filter_opis_nam;
    this.filter_delo = this.globalService.fileslistFilter.filter_delo;
    this.filter_filename = this.globalService.fileslistFilter.filter_filename;
    this.setOptions();
    try {
      if (!isNullOrUndefined(this.pageEvent)) {
        this.pageEvent.pageIndex = 0;
      }
      await this.setOrgerStr();
      await this.updateData();
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      if (+this.globalService.fileslistFilter.fileslistsetImgIdx >= 0) {
        this.onShowImgBack(+this.globalService.fileslistFilter.fileslistsetImgIdx +
          +this.globalService.fileslistFilter.fileslistsetImgIdxCount);
      }
    }
  }

  setOptions(arrowPrevIcon = 'fa fa-arrow-circle-o-left', arrowNextIcon = 'fa fa-arrow-circle-o-right') {
    this.galleryOptions = [
      {
        'imageAnimation': NgxGalleryAnimation.Fade,
        'arrowNextIcon': arrowNextIcon,
        'arrowPrevIcon': arrowPrevIcon,
        'previewZoom': true,
        'imageArrows': false,
        'previewRotate': false,
        'thumbnails': false,
        'image': false,
        'height': '1px',
        'actions': [
          {
            icon: 'fa fa-pencil-square-o',
            onClick: () => {
              this.globalService.fileslistFilter.fileslistsetImgId = this.setImgId;
              this.globalService.fileslistFilter.fileslistsetImgIdx = this.openImgIdx;
              this.globalService.fileslistFilter.fileslistsetImgIdxCount = this.openImgIdxCount;
              this.router.navigate([`/sys/files/edit/${this.setImgId}`]);
            },
            titleText: 'Редактировать'
          },
          {
            icon: 'fa fa-file-text-o',
            onClick: () => {
              this.globalService.fileslistFilter.fileslistsetImgId = this.setImgId;
              this.globalService.fileslistFilter.fileslistsetImgIdx = this.openImgIdx;
              this.globalService.fileslistFilter.fileslistsetImgIdxCount = this.openImgIdxCount;
              this.router.navigate([`/sys/listof/edit/${this.setListId}`]);
            },
            titleText: 'Перейти к делу'
          }
        ]
      }
    ];
  }

  selectOrder(type: string) {
    switch (type) {
      case 'byid':
        this.searchOrder = (this.searchOrder === 'byid') ? 'byiddesc' : 'byid';
        break;
      case 'bycrtime':
        this.searchOrder = (this.searchOrder === 'bycrtime') ? 'bycrtimedesc' : 'bycrtime';
        break;
      case 'byshifr':
        this.searchOrder = (this.searchOrder === 'byshifr') ? 'byshifrdesc' : 'byshifr';
        break;
      case 'byfilename':
        this.searchOrder = (this.searchOrder === 'byfilename') ? 'byfilenamedesc' : 'byfilename';
        break;
    }
    this.selectFilter(true);
  }

  selectFilter(event) {
    this.globalService.fileslistFilter.searchOrder = this.searchOrder;
    this.globalService.fileslistFilter.filter_fond = this.filter_fond;
    this.globalService.fileslistFilter.filter_opis_nam = this.filter_opis_nam;
    this.globalService.fileslistFilter.filter_delo = this.filter_delo;
    this.globalService.fileslistFilter.filter_filename = this.filter_filename;
    this.setOrgerStr();
    this.updateData();
  }

  async updateData() {
    this.globalService.fileslistFilter.searchOrder = this.searchOrder;
    this.globalService.fileslistFilter.filter_fond = this.filter_fond;
    this.globalService.fileslistFilter.filter_opis_nam = this.filter_opis_nam;
    this.globalService.fileslistFilter.filter_delo = this.filter_delo;
    this.globalService.fileslistFilter.filter_filename = this.filter_filename;
    try {
      let listfiles;
      let length;
      listfiles = this.listfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        0,
        this.pageSize,
        this.searchOrder
      );
      length = this.listfileService.getWithFilters_count(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename
      );
      this.length = (await length).rowcount;
      this.paginator.firstPage();
      this.listfiles = (isNullOrUndefined(await listfiles)) ? [] : await listfiles;
    } catch (err) {
      console.error(err);
    }
  }

  setOrgerStr() {
    switch (this.searchOrder) {
      case 'byid':
        this.postfixId = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixCrtime = '';
        this.postfixShifr = '';
        this.postfixFilename = '';
        break;
      case 'byiddesc':
        this.postfixId = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixCrtime = '';
        this.postfixShifr = '';
        this.postfixFilename = '';
        break;
      case 'bycrtime':
        this.postfixId = '';
        this.postfixCrtime = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixShifr = '';
        this.postfixFilename = '';
        break;
      case 'bycrtimedesc':
        this.postfixId = '';
        this.postfixCrtime = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixShifr = '';
        this.postfixFilename = '';
        break;
      case 'byshifr':
        this.postfixId = '';
        this.postfixCrtime = '';
        this.postfixShifr = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixFilename = '';
        break;
      case 'byshifrdesc':
        this.postfixId = '';
        this.postfixCrtime = '';
        this.postfixShifr = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixFilename = '';
        break;
      case 'byfilename':
        this.postfixId = '';
        this.postfixCrtime = '';
        this.postfixShifr = '';
        this.postfixFilename = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'byfilenamedesc':
        this.postfixId = '';
        this.postfixCrtime = '';
        this.postfixShifr = '';
        this.postfixFilename = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
    }
  }

  async setPageSizeOptions(event: PageEvent) {
    this.start = event.pageSize * event.pageIndex;
    try {
      let listfiles = this.listfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        this.start,
        event.pageSize,
        this.searchOrder
      );
      this.listfiles = (isNullOrUndefined(await listfiles)) ? [] : await listfiles;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }
    this.pageSize = event.pageSize;
    this.pageEvent = event;
  }

  isImgtype(filename: string): boolean {
    let res = false;
    switch (filename.toLowerCase().split('.').reverse()[0]) {
      case 'png':
      case 'jpg':
      case 'jpeg':
      case 'bmp':
        res = true;
        break;
      default:
        res = false;
        break;
    }
    return res;
  }

  async onShowImgBack(idx) {
    this.openImgIdx = idx;
    this.setImgId = +this.globalService.fileslistFilter.fileslistsetImgId;
    let plistfiles: any[];
    let nlistfiles: any[];
    let listfiles: any[];
    try {
      let _plistfiles = this.listfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        idx,
        1000 + 1,
        this.searchOrder,
        true
      );
      let _nlistfiles = this.listfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        ((idx - 1000) > 0) ? idx - 1000 : 0,
        1000,
        this.searchOrder,
        true
      );
      plistfiles = (isNullOrUndefined(await _plistfiles)) ? [] : await _plistfiles;
      nlistfiles = (isNullOrUndefined(await _nlistfiles)) ? [] : await _nlistfiles;
      this._listfiles = [...nlistfiles, ...plistfiles];
      let used = {};
      this._listfiles = this._listfiles.filter(obj => {
        return obj.id in used ? 0 : (used[obj.id] = 1);
      });

      let _idx = this._listfiles.findIndex(item => item.id === this.setImgId);
      listfiles = this._listfiles.slice((_idx - this.imgBufsize) > 0 ? _idx - this.imgBufsize : 0,
        ((_idx + this.imgBufsize) < this._listfiles.length) ? _idx + this.imgBufsize : this._listfiles.length);
    } catch (err) {
      console.error(err);
    }
    this.galleryImages.length = 0;
    if (this.galleryImages.length === 0) {
      this.inProgress = true;
      this.inProgressVal = 0;
      this.inProgressVal += (100 / listfiles.length);
      await Promise.all(listfiles.map((item) => {
        return new Promise(async resolve => {
          let arr = item.listfile.filename.split('.');
          let type = arr[arr.length - 1];
          let resfile;
          switch (type.toLowerCase()) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'bmp':
              let file = await this.listfileService.getFileById(item.id);
              this.inProgressVal += (100 / listfiles.length);
              resfile = new Blob([file], { type: 'image/jpg' });
              let reader = new FileReader();
              reader.readAsDataURL(resfile);
              reader.onloadend = () => {
                let base64data = String(reader.result);
                this.galleryImages.push({
                  small: base64data,
                  medium: base64data,
                  big: base64data,
                  description: item.listfile.filename,
                  label: `${item.id}_!!%_${item.fond}_!!%_${item.creation_datetime}_!!%_${item.list_id}`
                });
                switch (this.searchOrder) {
                  case 'byid':
                    this.galleryImages.sort((a, b) => +a.label.split('_!!%_')[0] - +b.label.split('_!!%_')[0]);
                    break;
                  case 'byiddesc':
                    this.galleryImages.sort((a, b) => +b.label.split('_!!%_')[0] - +a.label.split('_!!%_')[0]);
                    break;
                  case 'bycrtime':
                    this.galleryImages.sort((a, b) => {
                      if (new Date(a.label.split('_!!%_')[2]) > new Date(b.label.split('_!!%_')[2])) {
                        return 1;
                      } else {
                        return -1;
                      }
                    });
                    break;
                  case 'bycrtimedesc':
                    this.galleryImages.sort((a, b) => {
                      if (new Date(a.label.split('_!!%_')[2]) < new Date(b.label.split('_!!%_')[2])) {
                        return 1;
                      } else {
                        return -1;
                      }
                    });
                    break;
                  case 'byshifr':
                    this.galleryImages.sort((a, b) => +a.label.split('_!!%_')[1] - +b.label.split('_!!%_')[1]);
                    break;
                  case 'byshifrdesc':
                    this.galleryImages.sort((a, b) => +b.label.split('_!!%_')[1] - +a.label.split('_!!%_')[1]);
                    break;
                  case 'byfilename':
                    this.galleryImages.sort((a, b) => (a.description > b.description) ? 1 : -1);
                    break;
                  case 'byfilenamedesc':
                    this.galleryImages.sort((a, b) => (a.description < b.description) ? 1 : -1);
                    break;
                }
                resolve();
              };
              break;
            default:
              resolve();
              break;
          }
        });
      }));
      this.inProgress = false;
    }
    setTimeout(() => {
      this.showGallary = true;
      this.setImgIdx = this.galleryImages.findIndex(item => +item.label.split('_!!%_')[0] === this.setImgId);
      this.gallary.openPreview(this.setImgIdx);
    }, 300);
  }

  async onShowImg(idx) {
    this.openImgIdx = this.start + idx;
    let id = this.listfiles[idx].id;
    this.setImgId = id;
    let plistfiles: any[];
    let nlistfiles: any[];
    let listfiles: any[];
    try {
      let _plistfiles = this.listfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        this.start + idx,
        1000 + 1,
        this.searchOrder,
        true
      );
      let _nlistfiles = this.listfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        ((this.start + idx - 1000) > 0) ? this.start + idx - 1000 : 0,
        1000,
        this.searchOrder,
        true
      );
      plistfiles = (isNullOrUndefined(await _plistfiles)) ? [] : await _plistfiles;
      nlistfiles = (isNullOrUndefined(await _nlistfiles)) ? [] : await _nlistfiles;
      this._listfiles = [...nlistfiles, ...plistfiles];
      let used = {};
      this._listfiles = this._listfiles.filter(obj => {
        return obj.id in used ? 0 : (used[obj.id] = 1);
      });
      let _idx = this._listfiles.findIndex(item => item.id === id);
      listfiles = this._listfiles.slice((_idx - this.imgBufsize) > 0 ? _idx - this.imgBufsize : 0,
        ((_idx + this.imgBufsize) < this._listfiles.length) ? _idx + this.imgBufsize : this._listfiles.length);
    } catch (err) {
      console.error(err);
    }
    this.galleryImages.length = 0;
    if (this.galleryImages.length === 0) {
      this.inProgress = true;
      this.inProgressVal = 0;
      this.inProgressVal += (100 / listfiles.length);
      await Promise.all(listfiles.map((item) => {
        return new Promise(async resolve => {
          let arr = item.listfile.filename.split('.');
          let type = arr[arr.length - 1];
          let resfile;
          switch (type.toLowerCase()) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'bmp':
              let file = await this.listfileService.getFileById(item.id);
              this.inProgressVal += (100 / listfiles.length);
              resfile = new Blob([file], { type: 'image/jpg' });
              let reader = new FileReader();
              reader.readAsDataURL(resfile);
              reader.onloadend = () => {
                let base64data = String(reader.result);
                this.galleryImages.push({
                  small: base64data,
                  medium: base64data,
                  big: base64data,
                  description: item.listfile.filename,
                  label: `${item.id}_!!%_${item.fond}_!!%_${item.creation_datetime}_!!%_${item.list_id}`
                });
                switch (this.searchOrder) {
                  case 'byid':
                    this.galleryImages.sort((a, b) => +a.label.split('_!!%_')[0] - +b.label.split('_!!%_')[0]);
                    break;
                  case 'byiddesc':
                    this.galleryImages.sort((a, b) => +b.label.split('_!!%_')[0] - +a.label.split('_!!%_')[0]);
                    break;
                  case 'bycrtime':
                    this.galleryImages.sort((a, b) => {
                      if (new Date(a.label.split('_!!%_')[2]) > new Date(b.label.split('_!!%_')[2])) {
                        return 1;
                      } else {
                        return -1;
                      }
                    });
                    break;
                  case 'bycrtimedesc':
                    this.galleryImages.sort((a, b) => {
                      if (new Date(a.label.split('_!!%_')[2]) < new Date(b.label.split('_!!%_')[2])) {
                        return 1;
                      } else {
                        return -1;
                      }
                    });
                    break;
                  case 'byshifr':
                    this.galleryImages.sort((a, b) => +a.label.split('_!!%_')[1] - +b.label.split('_!!%_')[1]);
                    break;
                  case 'byshifrdesc':
                    this.galleryImages.sort((a, b) => +b.label.split('_!!%_')[1] - +a.label.split('_!!%_')[1]);
                    break;
                  case 'byfilename':
                    this.galleryImages.sort((a, b) => (a.description > b.description) ? 1 : -1);
                    break;
                  case 'byfilenamedesc':
                    this.galleryImages.sort((a, b) => (a.description < b.description) ? 1 : -1);
                    break;
                }
                resolve();
              };
              break;
            default:
              resolve();
              break;
          }
        });
      }));
      this.inProgress = false;
    }
    setTimeout(() => {
      this.showGallary = true;
      this.setImgIdx = this.galleryImages.findIndex(item => +item.label.split('_!!%_')[0] === id);
      this.gallary.openPreview(this.setImgIdx);
    }, 300);
  }

  async addPrevImg() {
    let firstId = this.galleryImages[0].label.split('_!!%_')[0];
    let idx = this._listfiles.findIndex(item => item.id === +firstId);
    if (idx === 0 || idx === -1) {
      return false;
    } else {
      let file = await this.listfileService.getFileById(this._listfiles[idx - 1].id);
      // console.log('got prev')
      // this.setOptions();
      let resfile = new Blob([file], { type: 'image/jpg' });
      let reader = new FileReader();
      reader.readAsDataURL(resfile);
      reader.onloadend = () => {
        let base64data = String(reader.result);
        this.galleryImages.unshift({
          small: base64data,
          medium: base64data,
          big: base64data,
          description: this._listfiles[idx - 1].listfile.filename,
          // tslint:disable-next-line:max-line-length
          label: `${this._listfiles[idx - 1].id}_!!%_${this._listfiles[idx - 1].fond}_!!%_${this._listfiles[idx - 1].creation_datetime}_!!%_${this._listfiles[idx - 1].list_id}`
        });
      };
      return true;
    }
  }

  async addNextImg() {
    let lastId = this.galleryImages[this.galleryImages.length - 1].label.split('_!!%_')[0];
    let idx = this._listfiles.findIndex(item => item.id === +lastId);
    if (idx > this._listfiles.length - 2 || idx === -1) {
      return false;
    } else {
      let file = await this.listfileService.getFileById(this._listfiles[idx + 1].id);
      this.setOptions();
      // console.log('got next')
      let resfile = new Blob([file], { type: 'image/jpg' });
      let reader = new FileReader();
      reader.readAsDataURL(resfile);
      reader.onloadend = () => {
        let base64data = String(reader.result);
        this.galleryImages.push({
          small: base64data,
          medium: base64data,
          big: base64data,
          description: this._listfiles[idx + 1].listfile.filename,
          // tslint:disable-next-line:max-line-length
          label: `${this._listfiles[idx + 1].id}_!!%_${this._listfiles[idx + 1].fond}_!!%_${this._listfiles[idx + 1].creation_datetime}_!!%_${this._listfiles[idx + 1].list_id}`
        });
      };
      return true;
    }
  }

  async previewChange(e) {
    if (this.setImgIdx > e.index) { // в какую сторону перелистывание
      this.openImgIdxCount--;
      let firstId = this.galleryImages[0].label.split('_!!%_')[0]; // есть ли что подгружать (добавить обновление списка !!!)
      let idx = this._listfiles.findIndex(item => item.id === +firstId);
      if (idx === 0 || idx === -1) {
        this.setImgIdx = e.index;
        this.gallary.openPreview(e.index);
      } else {
        this.setOptions('', 'fa fa-arrow-circle-o-right');
        this.addPrevImg();
        // this.galleryImages.pop();
        this.setImgIdx = e.index + 1;
        this.gallary.openPreview(e.index + 1); // т.к. добавили в начало списка индекс изменился
      }
    } else if (this.setImgIdx < e.index) {
      this.openImgIdxCount++;
      let lastId = this.galleryImages[this.galleryImages.length - 1].label.split('_!!%_')[0];
      let idx = this._listfiles.findIndex(item => item.id === +lastId);
      if (idx === this._listfiles.length - 2 || idx === -1) {
        this.setImgIdx = e.index;
        this.gallary.openPreview(e.index);
      } else {
        this.setOptions('fa fa-arrow-circle-o-left', '');
        this.addNextImg();
        // this.galleryImages.shift();
        this.setImgIdx = e.index;
        this.gallary.openPreview(e.index);
      }
    }
    // this.setImgIdx = e.index;
    this.setImgId = +e.image.label.split('_!!%_')[0]; // id текущей картинки
    this.setListId = +e.image.label.split('_!!%_')[3]; // id документа текущей картинки
  }

  onDownloadfile(id) {
    let fileName = '';
    for (let i = 0; i < this.listfiles.length; i++) {
      if (!isNullOrUndefined(this.listfiles[i])) {
        if (id === this.listfiles[i].id) {
          fileName = this.listfiles[i].listfile.filename;
        }
      }
    }
    try {
      let blob = this.listfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe(event => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round(100 * event.loaded / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }
  }

}
