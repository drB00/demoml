import { NgModule } from '@angular/core';

import { SystemRoutingModule } from './system-routing.module';
import { SystemComponent } from './system.component';
import { MusersListComponent } from './musers/musers-list/musers-list.component';
import { MuserEditComponent } from './musers/muser-edit/muser-edit.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { BreadcrumbComponent } from './shared/components/breadcrumbs/breadcrumb.component';
import { MusersComponent } from './musers/musers.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { SharedModule } from '../shared/shared.module';
import { MusersFilterPipe } from './shared/pipes/musers-filter.pipe';
import { DisablecontrolDirective } from './shared/directives/disablecontrol.directive';
import { DialogYesNoComponent } from './shared/components/dialogs/dialog-yes-no/dialog-yes-no.component';
import { ListofsComponent } from './listofs/listofs.component';
import { ListofsListComponent } from './listofs/listofs-list/listofs-list.component';
import { ListofsEditComponent } from './listofs/listofs-edit/listofs-edit.component';
import { GeoobjectsComponent } from './geoobjects/geoobjects.component';
import { FiledropDirective } from './shared/directives/filedrop.directive';
import { NgxGalleryModule } from 'ngx-gallery';
import { GeoobjectsListComponent } from './geoobjects/geoobjects-list/geoobjects-list.component';
import { NamePipe } from './shared/pipes/name.pipe';
import { DialogSetstringComponent } from './shared/components/dialogs/dialog-setstring/dialog-setstring.component';
import { DialogLocalityComponent } from './geoobjects/dialog-locality/dialog-locality.component';
import { FilesComponent } from './files/files.component';
import { FilesListComponent } from './files/files-list/files-list.component';
import { FilesEditComponent } from './files/files-edit/files-edit.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { EditorComponent } from './editor/editor.component';

@NgModule({
  entryComponents: [
    DialogYesNoComponent,
    DialogSetstringComponent,
    DialogLocalityComponent
  ],
  declarations: [
    DisablecontrolDirective,
    FiledropDirective,
    SystemComponent,
    MusersListComponent,
    MuserEditComponent,
    HeaderComponent,
    BreadcrumbComponent,
    MusersComponent,
    PageNotFoundComponent,
    MusersFilterPipe,
    DialogYesNoComponent,
    DialogSetstringComponent,
    DialogLocalityComponent,
    ListofsComponent,
    ListofsListComponent,
    ListofsEditComponent,
    GeoobjectsComponent,
    GeoobjectsListComponent,
    NamePipe,
    FilesComponent,
    FilesListComponent,
    FilesEditComponent,
    EditorComponent
  ],
  imports: [
    SharedModule,
    NgxGalleryModule,
    ImageCropperModule,
    SystemRoutingModule
  ]
})
export class SystemModule { }
