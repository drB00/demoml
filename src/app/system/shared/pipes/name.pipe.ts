import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {

  transform(items: any[], str: string): any {
    if (isNullOrUndefined(items) || str === '' || items.length === 0) {
      return items;
    }
    return items.filter((item) => {
      return item.name.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        item.id === +str;
    });
  }

}
