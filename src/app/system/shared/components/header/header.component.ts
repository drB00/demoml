import { Component, OnInit } from '@angular/core';
import { GlobalService, Link } from '../../services/global.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  mainMenu: Link[];
  isCollapsed = true;

  constructor(
    private globalService: GlobalService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.mainMenu = this.globalService.mainMenu;
  }

  gotoProfile() {

  }

  logout() {
    this.authService.logout();
  }

}
