/**Компонент для создания хлебных крошек
 * label из роутов
 */
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { BreadCrumb } from 'src/app/shared/models/breadcrumb.model';
import { of } from 'rxjs';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BreadcrumbComponent implements OnInit {
  breadcrumbs: BreadCrumb[];

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
      }
    });
  }

  ngOnInit() {
  }

  buildBreadCrumb(route: ActivatedRoute, url: string = '',
    breadcrumbs: Array<BreadCrumb> = []): Array<BreadCrumb> {
    // If no routeConfig is avalailable we are on the root path
    let label = route.routeConfig ? route.routeConfig.data['breadcrumb'] : '';
    let path = route.routeConfig ? route.routeConfig.path : '';
    // In the routeConfig the complete path is not available,
    // so we rebuild it each time
    let nextUrl = `${url}${path}/`;
    let breadcrumb = {
      label: label,
      url: nextUrl,
    };
    if (breadcrumb.label !== '') {
      breadcrumbs.push(breadcrumb);
    }
    let newBreadcrumbs = breadcrumbs;
    if (route.firstChild && route.firstChild.routeConfig.data) {
      // If we are not on our current path yet,
      // there will be more children to look after, to build our breadcumb
      return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
    }
    // if (newBreadcrumbs.length === 1) {
    //   newBreadcrumbs.length = 0;
    // }
    return newBreadcrumbs;
  }
}
