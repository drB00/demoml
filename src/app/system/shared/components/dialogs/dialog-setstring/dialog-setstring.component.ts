import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-setstring',
  templateUrl: './dialog-setstring.component.html',
  styleUrls: ['./dialog-setstring.component.css']
})
export class DialogSetstringComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogSetstringComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
