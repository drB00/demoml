import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ListService } from '../../shared/services/list.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { List } from 'src/app/shared/models/list.model';
import { isNullOrUndefined } from 'util';
import { GlobalService } from '../../shared/services/global.service';
import { DialogYesNoComponent } from '../../shared/components/dialogs/dialog-yes-no/dialog-yes-no.component';
import {
  NgxGalleryOptions,
  NgxGalleryImage,
  NgxGalleryComponent
} from 'ngx-gallery';
import { ListfileService } from '../../shared/services/listfile.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { saveAs as importedSaveAs } from 'file-saver';
import { ListlocalityService } from '../../shared/services/listlocality.service';
import { Locality } from 'src/app/shared/models/locality.model';
import { Observable } from 'rxjs';
import { LocalityService } from '../../shared/services/locality.service';
import { startWith, map } from 'rxjs/operators';
import { DialogLocalityComponent } from '../../geoobjects/dialog-locality/dialog-locality.component';

@Component({
  selector: 'app-listofs-edit',
  templateUrl: './listofs-edit.component.html',
  styleUrls: ['./listofs-edit.component.scss']
})
export class ListofsEditComponent implements OnInit {
  id: number | any;
  intType: any = ''; // тип интерфейса
  viewOnly: boolean; // тип интерфейса

  selectedList: List;
  selectedListfiles = [];
  files = [];
  @ViewChild('filesInput') filesInput: ElementRef;

  listForm: FormGroup;

  inProgress = false;
  inProgressVal = 0;
  /**Галерея */
  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];
  @ViewChild('gallary') gallary: NgxGalleryComponent;
  showGallary = true;

  listlocalitys = [];
  selectedlistlocalitys = [];
  listlocalityLinks = [];
  newlistlocalityLinks = [];
  listlocalitysId = [];
  listlocalityControl = new FormControl();
  timerId;
  filteredLocalitys: Observable<Locality[]>;

  constructor(
    private authService: AuthService,
    private globalService: GlobalService,
    private listService: ListService,
    private localityService: LocalityService,
    private listlocalityService: ListlocalityService,
    private listfileService: ListfileService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activatedRoute.params.subscribe(param => {
      this.id = param.id;
      if (param.id === 'newdoc') {
        this.intType = 'newdoc';
      } else {
        this.id = +param.id;
        this.intType = 'editdoc';
      }
    });
  }

  async ngOnInit() {
    this.galleryOptions = [
      { previewZoom: true, previewRotate: true },
      { thumbnails: false },
      { image: false, height: '1px' }
    ];
    this.listForm = new FormGroup({
      rtype: new FormControl(0),
      archive: new FormControl(null),
      fond: new FormControl(null),
      opis: new FormControl(null),
      delo: new FormControl(null),
      sheets: new FormControl(null),
      preparation_date: new FormControl(null)
    });
    await this.getData();
  }

  async getData() {
    switch (this.intType) {
      case 'newdoc':
        this.listForm.patchValue({
          rtype: 0,
          archive: null,
          fond: null,
          opis: null,
          delo: null,
          sheets: null,
          preparation_date: null
        });
        break;
      case 'editdoc':
        let _listlocalitysId = this.listlocalityService.getByList(this.id);
        this.selectedList = await this.listService.getOneById(this.id);
        let selectedListfiles = this.listfileService.getAllByParentId(this.id);
        this.selectedListfiles = isNullOrUndefined(await selectedListfiles)
          ? []
          : await selectedListfiles;

        this.selectedlistlocalitys = isNullOrUndefined(await _listlocalitysId)
          ? []
          : await _listlocalitysId;
        this.listlocalitysId = this.selectedlistlocalitys.map(
          item => item.locality_id
        );
        let used = {};
        this.listlocalitysId = this.listlocalitysId.filter(obj => {
          return obj in used ? 0 : (used[obj] = 1);
        });

        let listlocalityLinks = this.localityService.getByIds(
          this.listlocalitysId
        );
        this.listlocalityLinks = isNullOrUndefined(await listlocalityLinks)
          ? []
          : await listlocalityLinks;
        this.listlocalityLinks.map(item => {
          item.checkToDel = false;
        });
        this.selectedListfiles.map(
          selectedListfile => (selectedListfile.checkToDel = false)
        );
        this.selectedListfiles.sort((a, b) =>
          a.listfile.filename > b.listfile.filename ? 1 : -1
        );
        if (!isNullOrUndefined(this.selectedList)) {
          this.listForm.patchValue({
            rtype: this.selectedList.rtype,
            archive: this.selectedList.archive,
            fond: this.selectedList.fond,
            opis: this.selectedList.opis,
            delo: this.selectedList.delo,
            sheets: this.selectedList.sheets,
            preparation_date: this.selectedList.preparation_date
            // 'preparation_date': this.globalService.dateFromDateString(this.selectedList.preparation_date)
          });
        }
        break;
      default:
        break;
    }
  }

  resetpreparation_date() {
    this.listForm.patchValue({
      preparation_date: ''
    });
  }

  async onDelMuser() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить список?'
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === true) {
        try {
          let resDel = await this.listService.deleteOneById(this.id);
          this.router.navigate(['/sys/listof/list']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении списка ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      }
    });
  }

  async onSaveListform() {
    // if (!isNullOrUndefined(this.listForm.value['preparation_date']) && this.listForm.value['preparation_date'] !== '') {
    //   if (!(new Date(this.listForm.value['preparation_date']) instanceof Date &&
    //     !isNaN(new Date(this.listForm.value['preparation_date']).valueOf()))) {
    //     this.openSnackBar(`Введена не существующая дата смерти`, 'Ok');
    //     return;
    //   }
    // } else {
    //   this.listForm.patchValue({
    //     'preparation_date': null
    //   });
    // }
    switch (this.intType) {
      case 'newdoc':
        try {
          let resPostMuser = await this.listService.postOne(
            this.listForm.value
          );
          this.inProgressVal += 100 / this.files.length;
          let addListfiles = Promise.all(
            this.files.map(async file => {
              let fileForm: FormData = new FormData();
              fileForm.append('list_id', resPostMuser.id.toString());
              fileForm.append('listfile', file);
              await this.listfileService.postOne(fileForm);
              this.inProgressVal += 100 / this.files.length;
            })
          );
          let addListlocalityLinks = Promise.all(
            this.newlistlocalityLinks.map(async link => {
              let check = this.listlocalitysId.find(
                item => item.locality_id === link.id
              );
              if (isNullOrUndefined(check)) {
                await this.listlocalityService.postOne({
                  locality_id: link.id,
                  list_id: resPostMuser.id
                });
              }
            })
          );
          await addListlocalityLinks;
          await addListfiles;
          this.openSnackBar(`Список добавлен.`, 'Ok');
          this.router.navigate(['/sys/listof/list']);
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
        break;
      case 'editdoc':
        try {
          await this.listService.putOneById(this.id, this.listForm.value);
          let deleteSelectedListfiles = Promise.all(
            this.selectedListfiles.map(async selectedListfile => {
              if (selectedListfile.checkToDel) {
                await this.listfileService.deleteOneById(selectedListfile.id);
              }
            })
          );
          this.inProgressVal += 100 / this.files.length;
          let addListfiles = Promise.all(
            this.files.map(async file => {
              let fileForm: FormData = new FormData();
              fileForm.append('list_id', this.id.toString());
              fileForm.append('listfile', file);
              await this.listfileService.postOne(fileForm);
              this.inProgressVal += 100 / this.files.length;
            })
          );
          let deleteListlocalityLinks = Promise.all(
            this.listlocalityLinks.map(async listlocalityLink => {
              if (listlocalityLink.checkToDel === true) {
                let listlocality = this.selectedlistlocalitys.find(
                  item => item.locality_id === listlocalityLink.id
                );
                let deleteListlocalityLink = await this.listlocalityService.deleteOneById(
                  listlocality.id
                );
              }
            })
          );
          let addListlocalityLinks = Promise.all(
            this.newlistlocalityLinks.map(async link => {
              let res = await this.listlocalityService.postOne({
                locality_id: link.id,
                list_id: this.id
              });
            })
          );

          await deleteSelectedListfiles;
          await addListfiles;
          await deleteListlocalityLinks;
          await addListlocalityLinks;
          this.newlistlocalityLinks.length = 0;
          this.onDelFiles();
          this.getData();
          this.openSnackBar(`Сохранено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
        break;
      default:
        break;
    }
  }

  async onShowImg(id) {
    let fname: string;
    this.selectedListfiles.map(item => {
      if (item.id === id) {
        fname = item.listfile.filename;
      }
    });
    if (this.galleryImages.length === 0) {
      this.inProgress = true;
      this.inProgressVal = 0;
      this.inProgressVal += 100 / this.selectedListfiles.length;
      await Promise.all(
        this.selectedListfiles.map(item => {
          return new Promise(async resolve => {
            let arr = item.listfile.filename.split('.');
            let type = arr[arr.length - 1];
            let resfile;
            switch (type.toLowerCase()) {
              case 'png':
              case 'jpg':
              case 'jpeg':
              case 'bmp':
                let file = await this.listfileService.getFileById(item.id);
                this.inProgressVal += 100 / this.selectedListfiles.length;
                resfile = new Blob([file], { type: 'image/jpg' });
                let reader = new FileReader();
                reader.readAsDataURL(resfile);
                reader.onloadend = () => {
                  let base64data = String(reader.result);
                  this.galleryImages.push({
                    small: base64data,
                    medium: base64data,
                    big: base64data,
                    description: item.listfile.filename
                  });
                  this.galleryImages.sort((a, b) =>
                    a.description > b.description ? 1 : -1
                  );
                  resolve();
                };
                break;
              default:
                resolve();
                break;
            }
          });
        })
      );
      this.inProgress = false;
    }
    setTimeout(() => {
      this.showGallary = true;
      let idx = this.galleryImages.findIndex(
        item => item.description === fname
      );
      this.gallary.openPreview(idx);
    }, 300);
  }

  isImgtype(filename: string): boolean {
    let res = false;
    switch (
      filename
        .toLowerCase()
        .split('.')
        .reverse()[0]
    ) {
      case 'png':
      case 'jpg':
      case 'jpeg':
      case 'bmp':
        res = true;
        break;
      default:
        res = false;
        break;
    }
    return res;
  }

  onFilesChange(file: FileList) {
    this.files = this.files.concat(file);
  }

  onFilesChangeInput(event) {
    for (let i = 0; i < event.srcElement.files.length; i++) {
      this.files.push(event.srcElement.files[i]);
    }
  }

  onDelFiles() {
    this.files.length = 0;
  }

  onOpenFilesInput() {
    this.filesInput.nativeElement.click();
  }

  onDelFileById(id) {
    this.files.splice(id, 1);
  }

  toggleCheckToDel(id) {
    this.selectedListfiles[id].checkToDel = !this.selectedListfiles[id]
      .checkToDel;
  }

  async onDownloadListfile(id) {
    let fileName = '';
    for (let i = 0; i < this.selectedListfiles.length; i++) {
      if (!isNullOrUndefined(this.selectedListfiles[i])) {
        if (id === this.selectedListfiles[i].id) {
          fileName = this.selectedListfiles[i].listfile.filename;
        }
      }
    }
    try {
      let blob = this.listfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe(event => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }
  }

  toggleCheckToDelLink(id) {
    this.listlocalityLinks[id].checkToDel = !this.listlocalityLinks[id]
      .checkToDel;
  }

  displayFn(locality?: Locality): string | undefined {
    return locality && locality.name
      ? `${locality.republic_name} - ${locality.region_name} - ${locality.district_name} - ${locality.name}`
      : '';
  }

  async onAddArchivefolderlink() {
    this.newlistlocalityLinks.push(this.listlocalityControl.value);
    this.listlocalitys = this.listlocalitys.filter(
      item => item.id !== this.listlocalityControl.value.id
    );
    this.listlocalityControl.patchValue({});
  }

  onDelLinkById(id) {
    this.listlocalitys = this.listlocalitys.concat(
      this.newlistlocalityLinks.splice(id, 1)
    );
    this.listlocalityControl.patchValue({});
  }

  inputLink() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.listlocalitys = await this.localityService.getWithFilter(
        100,
        this.listlocalityControl.value,
        this.listForm.value.rtype
      );
      if (!isNullOrUndefined(this.listlocalitys)) {
        try {
          this.listlocalitys = this.listlocalitys.filter(
            listlocality => listlocality.list_id !== this.id
          );
          this.listlocalityLinks.map(link => {
            this.listlocalitys = this.listlocalitys.filter(
              listlocality => listlocality.id !== link.id
            );
          });
          this.newlistlocalityLinks.map(link => {
            this.listlocalitys = this.listlocalitys.filter(
              listlocality => listlocality.id !== link.id
            );
          });
          this.filteredLocalitys = this.listlocalityControl.valueChanges.pipe(
            startWith<string | Locality>(''),
            map(value => (typeof value === 'string' ? value : value.name)),
            map(name =>
              name ? this._filter(name) : this.listlocalitys.slice()
            )
          );
        } catch {}
      }
    }, 500);
  }

  private _filter(name: string): Locality[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.listlocalitys)
      ? null
      : this.listlocalitys.filter(
          listlocality =>
            listlocality.name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  async onViewLocality(id) {
    this.galleryImages.length = 0;
    let locality = await this.localityService.getOneById(id);
    let dialogRef = this.dialog.open(DialogLocalityComponent, {
      panelClass: 'app-dialog-full',
      data: {
        title: `Просмотр населенного пункта в районе ${locality.district_name}`,
        myFlagForButtonToggle: 'map',
        id,
        viewOnly: true,
        edit: true,
        name: locality.name,
        district_center: locality.district_center,
        longitude: locality.longitude,
        latitude: locality.latitude
      }
    });
    dialogRef.afterClosed().subscribe(async result => {});
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000
    });
  }

  async previewChange(e) {
    // console.log(e)
    // await Promise.all(this.selectedListfiles.map((item) => {
    //   return new Promise(async resolve => {
    //     let arr = item.listfile.filename.split('.');
    //     let type = arr[arr.length - 1];
    //     let resfile;
    //     switch (type.toLowerCase()) {
    //       case 'png':
    //       case 'jpg':
    //       case 'jpeg':
    //       case 'bmp':
    //         let file = await this.listfileService.getFileById(item.id);
    //         this.inProgressVal += (100 / this.selectedListfiles.length);
    //         resfile = new Blob([file], { type: 'image/jpg' });
    //         let reader = new FileReader();
    //         reader.readAsDataURL(resfile);
    //         reader.onloadend = () => {
    //           let base64data = String(reader.result);
    //           this.galleryImages.push({
    //             small: base64data,
    //             medium: base64data,
    //             big: base64data,
    //             description: item.listfile.filename,
    //           });
    //           this.galleryImages.sort((a, b) => (a.description > b.description) ? 1 : -1);
    //           resolve();
    //         };
    //         break;
    //       default:
    //         resolve();
    //         break;
    //     }
    //   });
    // }));
  }

  checkType(e, type) {
    if (this.listForm.value.rtype !== +type) {
      this.listlocalityControl.patchValue({});
      this.filteredLocalitys = null;
      if (
        !isNullOrUndefined(this.listlocalityLinks) &&
        this.listlocalityLinks.length > 0
      ) {
        if (this.listlocalityLinks[0].rtype !== +type) {
          this.openSnackBar(
            `Для смены типа списка удалите привязку к населенным пунктам.`,
            'Ok'
          );
          e.preventDefault();
          e.stopPropagation();
          return;
        }
      }
      if (
        !isNullOrUndefined(this.newlistlocalityLinks) &&
        this.newlistlocalityLinks.length > 0
      ) {
        if (this.newlistlocalityLinks[0].rtype !== +type) {
          this.openSnackBar(
            `Для смены типа списка удалите привязку к населенным пунктам.`,
            'Ok'
          );
          e.preventDefault();
          e.stopPropagation();
          return;
        }
      }
    }
  }
}
