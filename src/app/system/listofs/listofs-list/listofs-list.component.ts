import { Component, OnInit, ViewChild } from '@angular/core';
import { List } from 'src/app/shared/models/list.model';
import { PageEvent, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { ListService } from '../../shared/services/list.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MuserService } from '../../shared/services/muser.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GlobalService } from '../../shared/services/global.service';
import { isNullOrUndefined } from 'util';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Locality } from 'src/app/shared/models/locality.model';
import { LocalityService } from '../../shared/services/locality.service';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-listofs-list',
  templateUrl: './listofs-list.component.html',
  styleUrls: ['./listofs-list.component.scss']
})
export class ListofsListComponent implements OnInit {

  searchOrder = 'byid';
  postfixId = '';
  postfixArchive = '';
  postfixShifr = '';

  searchStr = '';
  searchStr_fond = '';
  searchStr_opis = '';
  searchStr_delo = '';

  // loggedInUser: Muser;
  lists: List[];

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent;
  start = 0;
  end = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  listlocalitys: Locality[] = [];
  listlocalityControl = new FormControl();
  timerId;
  filteredLocalitys: Observable<Locality[]>;
  selectedLocalityId = -1;

  constructor(
    private authService: AuthService,
    private localityService: LocalityService,
    private globalService: GlobalService,
    private listService: ListService,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  async ngOnInit() {
    try {
      if (!isNullOrUndefined(this.pageEvent)) {
        this.pageEvent.pageIndex = 0;
      }
      let length = this.listService.getAll_count();
      this.length = (await length).rowcount;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.searchOrder = isNullOrUndefined(this.globalService.listFilter.searchOrder) ? 'byid' :
        this.globalService.listFilter.searchOrder;
      this.searchStr = isNullOrUndefined(this.globalService.listFilter.searchStr) ? '' :
        this.globalService.listFilter.searchStr;
      this.setOrgerStr();
      this.updateData();
    }
  }

  async updateData() {
    this.globalService.listFilter = {
      searchStr: this.searchStr, searchOrder: this.searchOrder
    };
    try {
      let lists;
      let length;
      if (this.searchStr.length + this.searchStr_fond.length + this.searchStr_opis.length + this.searchStr_delo.length > 0) {
        lists =
          this.listService.getWithShortfilterOffsetLimitOrder(
            this.searchStr,
            this.searchStr_fond,
            this.searchStr_opis,
            this.searchStr_delo,
            0,
            this.pageSize,
            this.searchOrder,
            this.selectedLocalityId
          );
        length = this.listService.getShortfilter_count(
          this.searchStr,
          this.searchStr_fond,
          this.searchStr_opis,
          this.searchStr_delo,
          this.selectedLocalityId
        );
      } else {
        lists = this.listService.getWithOffsetLimitOrder(0, this.pageSize, this.searchOrder, this.selectedLocalityId);
        length = this.listService.getAll_count();
      }
      this.length = (await length).rowcount;
      this.paginator.firstPage();
      this.lists = (isNullOrUndefined(await lists)) ? [] : await lists;
    } catch (err) {
      console.error(err);
    }
  }

  async setPageSizeOptions(event: PageEvent) {
    let start = event.pageSize * event.pageIndex;
    try {
      let lists;
      if (this.searchStr.length + this.searchStr_fond.length + this.searchStr_opis.length + this.searchStr_delo.length > 0) {
        lists =
          this.listService.getWithShortfilterOffsetLimitOrder(
            this.searchStr,
            this.searchStr_fond,
            this.searchStr_opis,
            this.searchStr_delo,
            start,
            this.paginator.pageSize,
            this.searchOrder
          );
      } else {
        lists = this.listService.getWithOffsetLimitOrder(start, event.pageSize, this.searchOrder);
      }
      this.lists = (isNullOrUndefined(await lists)) ? [] : await lists;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }
    this.pageSize = event.pageSize;
    this.pageEvent = event;
  }

  onAddList() {
    this.router.navigate(['/sys/listof/edit/newdoc']);
  }

  onEditList(id) {
    this.router.navigate([`/sys/listof/edit/${id}`]);
  }

  selectOrder(type: string) {
    switch (type) {
      case 'byid':
        this.searchOrder = (this.searchOrder === 'byid') ? 'byiddesc' : 'byid';
        break;
      case 'byarchive':
        this.searchOrder = (this.searchOrder === 'byarchive') ? 'byarchivedesc' : 'byarchive';
        break;
      case 'byafod':
        this.searchOrder = (this.searchOrder === 'byafod') ? 'byafoddesc' : 'byafod';
        break;
    }
    this.selectFilter(true);
  }

  selectFilter(event) {
    this.globalService.listFilter = {
      searchStr: this.searchStr, searchOrder: this.searchOrder
    };
    this.setOrgerStr();
    this.updateData();
  }

  setOrgerStr() {
    switch (this.searchOrder) {
      case 'byid':
        this.postfixId = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixArchive = '';
        this.postfixShifr = '';
        break;
      case 'byiddesc':
        this.postfixId = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixArchive = '';
        this.postfixShifr = '';
        break;
      case 'byarchive':
        this.postfixId = '';
        this.postfixArchive = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixShifr = '';
        break;
      case 'byarchivedesc':
        this.postfixId = '';
        this.postfixArchive = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixShifr = '';
        break;
      case 'byafod':
        this.postfixId = '';
        this.postfixArchive = '';
        this.postfixShifr = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'byafoddesc':
        this.postfixId = '';
        this.postfixArchive = '';
        this.postfixShifr = '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
    }
  }

  inputLink() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.listlocalitys = await this.localityService.getWithFilter(100, this.listlocalityControl.value);
      if (!isNullOrUndefined(this.listlocalitys)) {
        try {
          this.filteredLocalitys = this.listlocalityControl.valueChanges
            .pipe(
              startWith<string | Locality>(''),
              map(value => typeof value === 'string' ? value : (!isNullOrUndefined(value)) ? value.name : null),
              map(name => name ? this._filter(name) : this.listlocalitys.slice())
            );
        } catch { }
      }
    }, 500);
  }

  resetLocality() {
    this.listlocalityControl.reset();
    this.listlocalitys.length = 0;
    this.selectedLocalityId = -1;
    try {
      this.filteredLocalitys = this.listlocalityControl.valueChanges
        .pipe(
          startWith<string | Locality>(''),
          map(value => typeof value === 'string' ? value : (!isNullOrUndefined(value)) ? value.name : null),
          map(name => name ? this._filter(name) : this.listlocalitys.slice())
        );
    } catch { } finally {
      this.updateData();
    }
  }

  selectLocality(locality) {
    this.selectedLocalityId = locality.id;
    this.updateData();
  }

  displayFn(locality?: Locality): string | undefined {
    return (locality && locality.name) ?
      `${locality.republic_name} - ${locality.region_name} - ${locality.district_name} - ${locality.name}` : '';
  }

  private _filter(name: string): Locality[] {
    const filterValue = name.toLowerCase();
    return (isNullOrUndefined(this.listlocalitys)) ? null :
      this.listlocalitys.filter(listlocality => listlocality.name.toLowerCase().indexOf(filterValue) !== -1);
  }


}
