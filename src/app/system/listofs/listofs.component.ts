import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-listofs',
  templateUrl: './listofs.component.html',
  styleUrls: ['./listofs.component.scss']
})
export class ListofsComponent implements OnInit {

  constructor(
    private router: Router,
  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url === '/sys/listof') {
          this.router.navigate(['/sys/listof/list']);
        }
      }
    });
  }

  ngOnInit() {
  }

}
