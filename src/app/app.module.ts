import { BrowserModule } from '@angular/platform-browser';
import {
  NgModule,
  LOCALE_ID,
  forwardRef,
  APP_INITIALIZER
} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import localeRu from '@angular/common/locales/ru';
registerLocaleData(localeRu);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AuthModule } from './auth/auth.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SystemModule } from './system/system.module';
import { MatPaginatorIntl } from '@angular/material';
import { MatPaginatorIntlRus } from './system/shared/services/global.service';
import { registerLocaleData } from '@angular/common';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ConfigService } from './shared/services/config.service';
import { environment } from 'src/environments/environment';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ParamInterceptor } from './system/shared/services/api.interceptor';
import { BaseService } from './system/shared/services/base.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    // AngularEditorModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    Ng4LoadingSpinnerModule.forRoot(),
    FormsModule,
    AuthModule,
    SystemModule,
    HttpClientModule
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [ConfigService],
      multi: true
    },
    BaseService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ParamInterceptor,
      multi: true
    },
    { provide: LOCALE_ID, useValue: 'ru' },
    { provide: 'pathStr', useValue: '' },
    { provide: 'fileStr', useValue: '' },
    { provide: 'parentStr', useValue: '' },
    {
      provide: MatPaginatorIntl,
      useClass: forwardRef(() => MatPaginatorIntlRus)
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
export function ConfigLoader(configService: ConfigService) {
  return () => configService.load(environment.configFile);
}
