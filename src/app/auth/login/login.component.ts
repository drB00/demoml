import { Component, OnInit } from '@angular/core';
import { Muser } from 'src/app/shared/models/muser.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AuthService } from 'src/app/shared/services/auth.service';
import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    public snackBar: MatSnackBar,
    private router: Router
  ) { }

  async ngOnInit() {
    this.form = new FormGroup({
      'login': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required]),
      'rmbMe': new FormControl(true)
    });
    let user: Muser = JSON.parse(window.localStorage.getItem('user'));
    if (user) {
      let _userData = await this.usersService.getByLoginPwd(user, true);
      this.authService.login(_userData);
      this.router.navigate(['/sys/listof']);
    }
  }

  async onSubmit() {
    if (this.form.invalid) {
      return;
    }
    let user = {
      login: this.form.value.login,
      password: this.form.value.password
    };
    try {
      let _userData = await this.usersService.getByLoginPwd(user, false);
      if (this.form.value.rmbMe) {
        window.localStorage.setItem('user', JSON.stringify(_userData.login));
      }
      this.authService.login(_userData);
      this.router.navigate(['/sys/listof']);
    } catch (error) {
      if (error.status === 401) {
        this.openSnackBar(`Логин или пароль не верны.`, 'Ok');
      } else {
        this.openSnackBar(`Ошибка соединения, попробуйте позже или обратитесь к системному администратору.`, 'Ok');
      }
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

}
